# 16 Frogs in Blacksburg

# Testing changes

## Added favicon, might want to change for a different one
## Added meta description to head
## Added page title to head
## Changed footer <a> link contrast for WCAG
## Added logo img alt attribute for WCAG
## Added lazy loading JavaScript for frog images for improved loading times
## Moved mapbox script and CSS out of head to just before the closing body tag, testing loading time improvments
## Added Bulma CSS external link as an interal, reduced
## Resized and optimized some frog images for mobile, testing site load times
## Disabled single finger touch scroll on map for mobile devices, double finger/pinch should still work

## Updated the clickedFrog = map.querySourceFeatures('frogs', filter: ["==", "shortname", frogId] so that it would first zoom out to include all frogs, wait for map idle, and than run the querySourceFeatures, to avoid it running while the map was still rendering and not including all the frogs. Removes the "geometry not found" error that would randomely pop up
## Added "Hide Map" button for when the map is at the top of the screen, hidden on mobile. When clicked it hides the map, and if there is enough space uses flexbox to place the frog cards side by side 4x4. Added JS so that if the card map icon is clicked the map is put back into place. Text changes on click from "Hide Map" to "Show map". Button is kept stickied above the map until clicked, when it is moved back to relative positioning at the top.

## Removed external font awesome link, added svg interal svg map icon. Helped load speeds
## Removed external mapbox CSS link, added CSS styles to main internal CSS file. Somewhat improved speed.
## Added "preload" link for mapbox JS in head, tiny possible load time speed increase
## Add JS to put set rel attribute to "noopener" on two mapbox GL links after map load for two external links missing it.

This project is a public website for the 16 Frogs public art installation in Blacksburg, VA.
