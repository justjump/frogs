FROM jekyll/jekyll:4.0 AS jekyll

ENV JEKYLL_ENV production
COPY . /jekyll
WORKDIR /jekyll
RUN bundle install
RUN mkdir _site && mkdir .jekyll-cache && \
    jekyll build


FROM nginx
COPY --from=jekyll /jekyll/_site/ /usr/share/nginx/html/
