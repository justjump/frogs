---
name: Millie
shortname: millie
order: 7
blurb: I got my name from the building across the street, which used to be a mill. Stroubles Creek powered its millwheel. You might be able to hear the stream flowing beneath the steps next to me.
location: Armory steps, near 211 Draper Rd.
layout: frog
---

Listen at the steps to the Armory parking lot, and you might hear water coursing under your feet. Now routed below Draper Road, Stroubles Creek used to flood during heavy rains when it flowed above ground here, so this block developed more slowly than the rest of downtown. The first businesses on College Avenue took advantage of the gushing current. In the early 1900s, the Blacksburg Milling and Supply Company erected a roller mill driven by the stream. The creek was dammed near the corner of Jackson and Main, possibly to help power the mill. 

Equipped with five flour rollers and a stone wheel, the mill formed the rear wing of a 3.5-story structure, and a farm-supply store occupied the front. The company lent the street an industrial attitude that discouraged retail shops until the Lyric Theater moved to its present location in 1930. The stream was channeled underground around the same time. After World War II, the mill’s owners converted the upper floors into apartments.

The mill building still stands at the intersection with Draper Road, home to Gillie’s, the Rivermill, and the Community Arts Information Office. A brick facade now hides its massive timber frame. Stroubles Creek runs beneath its basement.

Take action! Dispose of pet waste in disposal stations at local parks and trails or with your household trash. Don’t flush it down the toilet.
