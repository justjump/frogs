---
name: E.D. Frog
shortname: edfrog
order: 15
blurb: I share my initials with the Eastern Divide, a curving boundary that stretches from New York to Florida. Rivers on the east side of the divide empty into the Atlantic Ocean, and rivers on the west side flow into the Gulf of Mexico. The divide runs right through Margaret Beeks Elementary!
location: Margaret Beeks Elementary, 709 Airport Rd.
layout: frog
---

Rain that falls on downtown Blacksburg will ultimately flow into the Gulf of Mexico. Rain that falls into the pond at First and Main will end up in the Atlantic. That’s because they’re on opposite sides of the eastern continental divide, a ridge line that separates streams flowing east from streams flowing west. The divide extends from southwestern New York State to the southern tip of Florida. In Blacksburg, it rises just east of Airport Road, cutting across the front lawn of Margaret Beeks Elementary and through the school’s gym and kitchen.

The mountains of the Alleghenies and the Blue Ridge bulge like pleats down the northern border of Virginia. Streams and travellers have long followed the path of least resistance through their parallel valleys. But along Blacksburg’s eastern edge, the ridgeline bends almost at a right angle to form a wall across the lowlands. This spur blocks Stroubles Creek from flowing into the Roanoke River. Instead, water from the north end of Blacksburg empties into the New River and, eventually, the Gulf of Mexico. Streams on the south end of town merge into a branch of the Roanoke River and make their way to the Atlantic.

Early travelers coming down the valley crossed the eastern divide at Cedar Run creek, near the current site of the Virginia Tech Transportation Institute, or at Indian Run creek, along what is now Harding Avenue in Ellett Valley. (Today locals know these routes as the “back ways” to Roanoke.) In 1763, King George III forbade his subjects from settling west of the divide, because that land had been reserved for Native Americans in the treaty ending the French and Indian War. The area that would become Blacksburg remained the frontier—briefly. Almost immediately, settlers ignored the boundary. William Preston built Smithfield west of the divide as part of a trend of growing indignation with the Crown’s control that would culminate in the American Revolution.

Take action! Recycle used motor oil and other automotive fluids.
