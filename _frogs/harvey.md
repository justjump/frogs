---
name: Harvey
shortname: harvey
order: 4
blurb: I’m named for Harvey Black, who helped found the college that became Virginia Tech. He lived next door, and the stream below me supplied his property with water.
location: Main Street Inn, 205 S. Main St.
layout: frog
---

A grandnephew of the founder of Blacksburg, Harvey Black decided as a boy to become a doctor. He apprenticed under two local physicians before graduating from the University of Virginia medical school in 1848, at age 21. After he returned home, he built a house on the site of what is now Mellow Mushroom, likely because of the lot’s easy access to water. The stream that now trickles past the Main Street Inn would have cut along the edge of his property. It begins a quarter mile east at Spout Spring, one of the early public sources of drinking water in town. 

Black opened a medical practice in Blacksburg in 1852. He also served as the president of the board of trustees of the Preston and Olin Institute, a small—and, by the early 1870s, insolvent—Methodist school for boys on Henderson Hill, near the entrance to the present-day Alumni Mall. When the state government began looking for a site for a new land-grant college, Black and Peter Whisner, the pastor of the Methodist Church, successfully lobbied the General Assembly to choose Blacksburg. Refurbishing the existing Preston and Olin campus would be cheaper than building a school from the ground up, and Montgomery Country residents voted to help pay to renovate the facilities. When the new institution, officially called the Virginia Agricultural and Mechanical College, opened in 1872, Black was the first rector of its Board of Visitors.

The same year, he was elected president of the Medical Society of Virginia. The governor soon asked him to act as superintendent of the Eastern Lunatic Asylum in Williamsburg, where, for six years, Black worked to improve conditions for patients. In 1885, he won a seat in the House of Delegates. There, he fought to have a mental hospital built in Marion, and he was named its first superintendent. He died in 1888, a year after beginning work in Marion.

Take action! Notify local government officials when you see sediment eroding into streets or streams near a construction site.
