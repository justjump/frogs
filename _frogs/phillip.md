---
name: Phillip
shortname: phillip
order: 2
blurb: I’m named after Phillip Price, one of the first African-American students to attend Blacksburg High School.
location: Spout Spring, Clay St. near Wharton St.
layout: frog
---

A thicket of trees and vines along Clay Street hides Spout Spring, one of three springs used for drinking water during Blacksburg’s first century. Buildings have been constructed over the other two, but Spout Spring still looks like it did when residents stopped by every day to fill their buckets. By the 1900s, it had replaced town spring, under what is now the Tech Bookstore on Main Street, as the town’s primary water source. Hollowed-out logs carried water from here to prominent homes in town, including Harvey Black’s on Main Street and the Thomas-Conner House on Draper Road. Spout Spring may have also been used to fill tanks on the Virginia Tech campus.

Spout Spring is the source of the stream that runs past Main Street Inn and under Kent Square. Beneath the pavilion in front of the Graduate Life Center, it meets the branch that parallels College Avenue. From there, the creek is channeled under the Drillfield before it re-emerges at the Duck Pond.

Just across Clay Street, a new Blacksburg High School building opened in January 1954. Four months later, the Supreme Court ruled in Brown vs. Board of Education that segregated public education was unconstitutional. Like many school systems in Virginia, Montgomery County defied the ruling. Blacksburg High served only whites for seven more years, until Phillip Price and his younger sister, Anna, became the first black students to enroll. Price had attended a segregated elementary school two blocks west on Clay Street, and another at the site of what is now Harding Avenue Elementary. Starting in eighth grade, he was bused to the Christiansburg Institute, which educated black high school students from 15 counties. In the spring of 1961, he and Anna petitioned to transfer to Blacksburg High School. They entered that fall.

Despite opposition to integration—a cross burned on the school grounds that spring—Price remembered his two years at Blacksburg High School as free of violence. The teachers and principal welcomed him, and white friends from his neighborhood walked with him to school. “You might hear comments in the hall,” he remembered. “You can stop and get upset or keep walking.” He took advanced classes and worked evenings as a ticket-taker at the Lyric and a janitor at a department store. Still, given the racial tension throughout the south, Price felt pressure as he plunged into an unfamiliar social environment. That experience managing stress helped him when, after graduating in 1963, he entered the Air Force. He went on to work for 18 years in a hospital pharmacy before becoming a Christian minister.

Take action! When washing your car, don’t let the water run into a storm drain. Dispose of suds in a utility sink, or better yet, wash your car at a facility where the drainage is connected to the sewer system.
