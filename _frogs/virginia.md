---
name: Virginia
shortname: virginia
order: 1
blurb: I’m named after Virginia Hummel, an English teacher who helped preserve the iris garden at the Price House.
location: Price House/SEEDS, 107 Wharton St.
layout: frog
---

For many years, when the irises bloomed at the Price House, a white-haired woman could be found every day working in the dirt along Lee Street. Virginia Hummel retired from a 35-year career in the Montgomery County school system in 1984. A year later, when the town acquired the Price House, she took over tending its extensive iris garden. Hummel had begun teaching English at Blacksburg High School in 1950. Energetic and exacting, she impressed the rules of grammar and the splendor of literature upon a generation of Blacksburg teenagers. After class, she volunteered as cheerleading coach, timekeeper at basketball games, faculty advisor to the school paper, and unofficial guidance counselor and confidant. In 1969 she became the supervisor of English instruction for Montgomery County Schools.

A Blacksburg native, Hummel had earned a bachelors in biology and a masters in zoology from Virginia Tech. She was partway through a doctoral program in cytology at the University of Virginia when she accepted a proposal from her high school sweetheart. The U.S. entered World War II two years later, and her husband joined up as a B-25 pilot. He was killed in a crash in a monsoon over Burma in 1944. Left to raise two daughters on her own, she navigated the demands of work and parenting while attending summer classes at the University of Minnesota. After eight years, she earned a masters in educational psychology in 1967. When she retired, she enrolled full-time in graduate-level classical studies at Virginia Tech. She also volunteered at the library, attended town council meetings, served on several committees, and researched local history.

Hummel urged the town to take ownership of the Price House in 1985 after Nelson Price offered it for preservation in his will. The oldest wing of the Price House—the log section that faces Wharton Street—was built before 1840. The property had changed hands several times by the time Walter Price, the superintendent of buildings at Virginia Tech, bought it with his wife, Lucie, in 1900. Their son Nelson was born in an upstairs bedroom in 1908. He inherited the house in 1954, and his wife, Jeanne, planted the iris garden in the 1960s. Nelson kept up with the weeding after she died, in 1977, but Hummel still had her work cut out for her when she took over. She led the group that redesigned the garden, sterilized the bed, and replanted the flowers. She supervised the weeding and spraying every year until she entered a nursing home in 1997. She died in 1998. “Heaven for me will be a library and eternity to enjoy it,” she had said, “with a little piece of ground beside it for digging and planting.”

The Price House lot sits on a shallow ridge that crosses downtown to the Alexander Black house. The ridge separates two tributaries of the Central Branch of Stroubles Creek: one that originates below the hills around the municipal golf course, and another that flows from springs on the northeastern end of town, near the old high school. They meet underground near the University Bookstore and continue beneath the Drillfield to the Duck Pond.

Take action to clean up Stroubles Creek! Use slightly composted leaves, compost, and mulch instead of chemical fertilizers to enhance your soil.
