---
name: Lindsay
shortname: lindsay
order: 10
blurb: I’m named after Lindsay West, the first woman elected to the Montgomery Board of Supervisors. 
location: Owens Park bridge, 316 Owens St.
layout: frog
---

The Central branch of Stroubles Creek runs straight through the middle of Owens Park, carrying water from the old Blacksburg High School property and the Apperson-Dickerson neighborhood. The park was designed to contain flood overflow during severe storms. From here, the stream meets tributaries from Harding Avenue, Roanoke Street, and Turner Street on its way to the Progress Street fire station and beyond.

Half a mile northwest of the park, the Webb branch of Stroubles cuts across the backyard of the Keister House on Giles Road, where Lindsay West lived for more than 50 years. West moved to Blacksburg with her husband, David, in 1962, and soon became a fierce advocate for civil rights, justice, and the arts in her adopted hometown. In 1976 she became the first woman elected to the Montgomery Board of Supervisors. She served as a supervisor for 11 years, including two terms as chair. After she left the board, she helped form the Community Foundation of the New River Valley and the local chapter of the League of Women Voters. As the first leader of the Lyric Council, she oversaw the theater’s renovation in the 1990s. She was active in a number of other civic groups, including the Montgomery Country Democratic Committee, which she chaired for nine years. She died in 2018. 

The Wests placed a preservation easement on their property, protecting the 1830s brick house and its 3.25 acres of woods from further development. Toward the back of the yard, under spreading black walnuts and sugar maples, stands Blacksburg’s last surviving spring house. The slope-roofed wooden shed covers one of three springs on the property that feed the Webb branch of Stroubles.

Take action! Don’t allow your dog to urinate or defecate near or in creeks or streams.
