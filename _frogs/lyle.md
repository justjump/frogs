---
name: Lyle
shortname: lyle
order: 14
blurb: I’m named for John Lyle, Sr., and John Lyle, Jr. They were a father and son who owned a school on this hill in the 1800s. In 1872 that school was converted into a college—and it grew to become Virginia Tech!
location: Moss Arts Center, 190 Alumni Mall
layout: frog
---

In 1850 local Methodists established the Olin and Preston Institute, a school for boys. Its three-story brick building stood a few hundred yards from the modern-day location of the Moss Center, looking south down Main Street. The structure was probably built by John Lyle, who owned the White Sulphur Springs resort east of town. Deep in debt, the trustees sold the school to Lyle by court order in 1859, possibly to settle claims of non-payment for the construction work. Lyle, according to lore, kept the school open because the woman he was courting refused to marry him otherwise. Olin and Preston finally closed when most of its teachers and students left to fight in the Civil War.

The school sat empty until 1869, when the Methodist minister Peter Whisner decided to revive it. By then, Lyle had died. His son, John Lyle Jr., sold the property to the new board of trustees, but he retained a lien. The school opened in 1869 as the Preston and Olin Institute.

Meanwhile, Virginia’s re-entry into the union allowed the state to receive federal funds under the Morrill Land Grant Act to establish a new agricultural and mechanical college. Twenty-four schools, including the University of Virginia, offered themselves as the ideal site. In a surprising move, the state legislature chose Preston & Olin, in what was then a rural backwater. Montgomery County residents had promised $20,000 to help build the college; the judge who ordered the referendum on whether to honor the pledge was Lyle, Jr. Lyle also gave the Preston & Olin trustees clear title to the property. That land, plus 250 acres bought from Col. Robert Preston, formed the campus of the Virginia Agricultural and Mechanical College. (Its name was changed to Virginia Agricultural and Mechanical College and Polytechnic Institute in 1896, and was shortened informally to Virginia Polytechnic Institute.)

The Olin and Preston building, the nucleus of the new institution, commanded the hill above downtown. At the time, Main Street veered out at an angle from its intersection with Jackson Street to meet Turner Street where it now curves behind the Moss Center. In 1874, in one of the college’s first transformative effects on the town, Main Street was rerouted directly toward the front steps of the school. From there the road jogged around the building’s northeast wall, tracing what we now call College Avenue Extended. When the structure burned to the ground, in 1913, the college responded by outlining a master plan to develop the rest of the campus. The plan suggested a new architectural signature, one that quickly became synonymous with Virginia Tech. The first neo-Gothic university building designed to be faced with limestone was constructed the next year. Within two and half decades, the Drillfield was ringed by what we now call Hokie Stone.

Take action! If you use yard chemicals, follow the product directions precisely to avoid over-application.
