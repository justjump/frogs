---
name: A.G.
shortname: ag
order: 6
blurb: I’m named after Archibald Gray Smith, II, the last person to live in the Five Chimneys house. Stroubles Creek runs in the open right below me—it’s one of the few places you can see the stream downtown.
location: Five Chimneys house, Draper Rd. near Washington St.
layout: frog
---

The park at Five Chimneys is one of the few places downtown that Stroubles Creek still flows above ground. The tributary here rolls down from the hills across Main Street, on its way to join the Central branch just before reaching the Drillfield. Draper Road was called Water Street into mid-1900s, because it followed the creekbed toward the heart of town. Blacksmiths worked at a shop across the street from Five Chimneys in the mid-1800s, using stream water to cool metalwork and control fire in the forge. Where Kent Square now stands, tanners and tinsmiths set up shop along the creek banks. In the early twentieth century, a trestle at the intersection of Draper Road and Clay Street carried railroad cars over the low-lying current. But as Blacksburg and the college grew over the next decades, most of the streams in the business district were diverted into culverts under buildings and parking lots.

Built around 1851, the house known as Five Chimneys was among the first in the area to be made from brick, at a time when most homes were log cabins. Its layout—two rooms on either side of a central hallway—was extremely rare in one-story homes in western Virginia, and it is the only brick example in Montgomery County. Most houses in towns of the era crowded at the edge of the street, leaving space in the back for animals and farming. But Five Chimneys sat behind an ornamental front yard, placing it in the vanguard of the emerging suburban ideal. (The stream may have prevented a more conventional position close to the road.)

The property passed through several owners before Archibald Gray Smith, II, bought it in 1936. Smith worked at VPI’s agricultural extension division, specializing in vegetable growing. Using his horticultural training, he laid out boxwood hedges and banks of rhododendron in the garden, creating room-like divisions that mimicked the landscapes of European estates. He also repaired and expanded the house, added concrete walkways to the yard, and put in the brick retaining wall along Washington Street. After his death, in 1986, the Town of Blacksburg purchased the property.

Take action! Have your car serviced regularly, and fix leaks promptly. Automotive fluids easily flow into storm drains and, eventually, streams.
