---
name: Keister
shortname: keister
order: 11
blurb: I’m named after the Keister family. The farm they owned in the 1800s was sold to make several neighborhoods east of Main Street. I sit above a branch of the creek that watered their land.
location: Between Kopy Korner and Citgo, Main St. and Progress St.
layout: frog
---

In 1874, Blacksburg grew beyond its original 16 squares for the first time. Keister’s addition, the town’s first subdivision, was laid out along Progress Street on land belonging to the Keister family. 

Frederick Keister sailed from Germany to the American colonies in 1737 and settled in Lancaster, Pennsylvania. His son Philip moved to Montgomery County in 1799 with his wife, Maria, and five children; they would add four more in their new home. These are the ancestors of all the Keisters around Blacksburg. They bought two tracts of land—111 acres of the Preston estate and 65 along Toms Creek. Philip’s oldest son, John, inherited or bought a significant chunk of the parcel in what would become Blacksburg. He probably built the 1830s brick house on Giles Road [see #10]; it augmented an earlier log cabin. John and his wife, Sally, passed the land to their ten children after they died. Their ninth-born, Christian bought out his siblings for the title to the Giles Road house and at least 14 acres around it, but John and Sally’s second-oldest, Jacob, ended up holding some of the most valuable real estate in town.

He sold land to create the Olin and Preston Institute in 1850; it was just west of where Main Street now meets College Avenue. In the 1870s and 1880s, he lived at the intersection of Jackson and Church Streets, where the Town Hall would be built in the 1920s. When Jacob died, in 1902, his U-shaped tract reached from what is today Harding Avenue Elementary north almost to the Caboose Park and west to the Wells Fargo bank downtown. The western arm stretched up Main and Kabrich Streets behind the current site of Gilbert Linkous Elementary to what is now Stonegate Drive. The north wall of the YMCA marks the property’s upper boundary on what became Main Street. The Bennett Hill/Progress, Apperson-Dickerson, and Kabrich Crescent neighborhoods were all carved from and mentioned in his estate. Streets and property lines in these subdivisions still follow the boundaries of the Keister tract.

The Webb branch of Stroubles Creek flows above ground for much of its length from the north end of Blacksburg. From here, it is piped to Webb Street, where it runs in the open until it crosses under Prices Fork.

Take action! Apply compost or mulch to keep your garden soil from washing away. 
