---
name: Bogen
shortname: bogen
order: 16
blurb: I’m named after Bill Ellenbogen, who worked to extend the Huckleberry Trail. Stroubles Creek flows right under the trail—and me!
location: Huckleberry Trail, just north of Southgate Dr. bridge
layout: frog
---

In the 1850s, the only road from Blacksburg to the new train station in Christiansburg was a nine-mile dirt wagon track. In good weather, the trip took 90 minutes. In the hills south of town, axle-deep mud could add another hour. For decades, entrepreneurs and coal companies promised to extend the rail line into Blacksburg. Tracks finally crossed into town in early September 1904, just in time for cadets to return from summer break. “Why, we are two hours closer to Christiansburg,” the VPI newspaper reported. Blacksburg’s first depot was a makeshift shack where the town library now stands. In 1912 Norfolk & Western built an improved station at the present site of the municipal building. 

The Merrimac Mine was an early stop on the railway. The coal company clear-cut the hillside forests around the mine, which allowed huckleberry bushes to flourish. The line was known as the Huckleberry by spring 1904, before it even pushed into Blacksburg. 

Cars and better roads had begun to make the line obsolete by the 1930s. Passenger service to Blacksburg ended in 1958. Freight service continued until 1966, when the tracks were removed to make room for a longer runway at the Virginia Tech airport. The depot was razed in 1967.

Within months, townspeople converted the right of way into a nature path between the library and Margaret Beeks. In 1989, Bill Ellenbogen proposed continuing the trail into Christiansburg. Ellenbogen, an offensive lineman on the Virginia Tech football team in the early 1970s, played in the NFL for seven years before returning to Blacksburg. He opened Bogen’s restaurant on North Main Street in 1982 and ran it for 18 years. He also became active in real estate, expanding University Mall, converting the former Grand Piano showroom downtown into Town Centre, and developing residential subdivisions. As head of the non-profit Friends of the Huckleberry Trail, Ellenbogen has been the trail’s chief advocate. He and the group raised millions of dollars for engineering and construction, coordinated efforts with government agencies, negotiated with property owners, got permission to cross Virginia Tech land, and organized volunteers who cleared brush and built bridges.

In 1998 the trail reached the New River Valley mall; it now continues south to Christiansburg High School. A northern spur links to the Jefferson National Forest. Montgomery County hopes to connect the trail with the Roanoke Valley’s greenways system and the New River Trail that runs from the town of Pulaski to Galax.

Buried below the underbrush just under half a mile from the trailhead, pipes carry an arm of the stream from headwaters near Palmer Drive. This tributary does not empty into the Duck Pond as the other branches do. It flows across the southern part of campus, joining Stroubles Creek in the pastures along route 460. From there it winds toward Radford, where it feeds into the New River at the horseshoe bend by the arsenal.

Take action! Never apply lawn chemicals or fertilizers just prior to a storm. Rain will strip the products from your lawn and wash them into our waterways.
