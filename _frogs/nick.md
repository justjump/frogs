---
name: Nick
shortname: nick
order: 8
blurb: I’m named after Nick Kappas, a Greek immigrant who opened the restaurant now called the Cellar.
location: Lyric Theater, 135 College Ave.
layout: frog
---

Nick Kappas, a Greek immigrant, travelled from his adopted home in Salem every Thanksgiving in the early 1900s to watch the VPI-VMI football game. One year, a group of cadets convinced him to open a restaurant in Blacksburg. He began serving meatloaf, vegetables, sandwiches, and chili in 1920 out of a building at the corner of Main Street and College Avenue. Kappas changed the official name of his business a few times before settling on what the townspeople had always called it: the Greek’s. For many years it was the only place near campus that sold alcohol. The restaurant is still open, now as the Cellar.

A few decades before Kappas first arrived, this block was the hub of town industry. In the nineteenth century, tanyards clustered along Blacksburg’s streams. Leather-making requires ample water for soaking hides, and local tanners were relieved to fill their vats from a source close by. The Cain-Miller tanyard sprawled across what is today the intersection of Main Street and College Avenue, on the banks of the branch that now runs under the Lyric. Other businesses soon moved in: A blacksmith, lumberyard, pottery kiln, and broom factory all eventually shared the tanyard grounds. At that time, the Central Branch of Stroubles flowed right across Main Street, by what is now Joe’s Diner. During heavy rains, the swelling stream turned the dirt road into a quagmire. A bridge was built over the crossing by 1912.

Take action! Research safer methods of weed and pest control to minimize the use of chemical pesticides and herbicides.
