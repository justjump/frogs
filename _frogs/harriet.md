---
name: Harriet
shortname: harriet
order: 3
blurb: I’m named after Harriet Dorsey, the first female lawyer in Montgomery County and the first female judge in the district.
location: CMG Leasing, Washington St. near Church St.
layout: frog
---

When Harriet Dorsey opened her private legal practice in Blacksburg in 1976, she became the first female attorney in Montgomery County. She had just graduated from Washington and Lee as a member of the second class at the university’s law school to include women. She worked tenaciously to establish her practice, which specialized in family law. For many years, her office was in the bungalow on the corner of Church and Washington.

In 1988, Dorsey was named a substitute judge in the Juvenile and Domestic Relations Court, hearing cases when full-time judges were unavailable. In 2009 the 27th Judicial Circuit Court appointed her to an interim seat in the same court. That made her the first woman judge in the district, which covers eight counties. Dorsey also lead the governing board for the New River Valley Juvenile Detention Home, served as president of the Mental Health Association of the New River Valley, and handled closings for Virginia Mountain Housing, a nonprofit agency that strives to build affordable homes. She is retired now and still lives in Blacksburg.

A small tributary emerges among the greenery here from beneath Washington Street, briefly visible above ground on its way to join the central branch of Stroubles Creek and empty into the Duck Pond. This stream originates two blocks to the east at Spout Spring, once Blacksburg’s main water supply. A previous owner of the property, William Watkins, preserved his stretch of the stream as a feature in his backyard, arranging chairs and bird feeders around the small pond.

Take action! Pick up after your pet. Pet waste contains bacteria that can pollute waterways. When walking your dog, carry plenty of waste bags.
