---
name: Gilbert Vaughn
shortname: gilbertvaughn
order: 13
blurb: I’m named in honor of the first black landowner in this neighborhood. Called New Town, it was home to about 20 black families starting in the late 1800s.
location: Odd Fellows Hall, 203 Gilbert St.
layout: frog
---

When Gilbert Vaughn paid $50 for half an acre of land in 1874, he became the first black landowner in the neighborhood known as New Town. Vaughn and his wife and three children had come to Montgomery County as slaves during the tobacco boom of the 1850s. Toward the end of Reconstruction, black families began to settle in several areas around Blacksburg. Some 20 families eventually moved to New Town, clustered along Gilbert Street, named after Vaughn, and a connecting alley. New Town was independent until Blacksburg annexed it in 1896.

Under segregation, the residents of New Town lacked the access to schools, jobs, and institutions that whites had. To compensate, they built a self-reliant, close-knit community. Adults looked after all the neighborhood children, families supported one another, and everyone gathered on weekends at the St. Luke and Odd Fellows Hall for dinners, dances, fashion shows, bingo parties, mock weddings, Easter egg hunts, or ball games. Entrepreneurs from New Town contributed to Blacksburg’s growing prosperity.

The Vaughns lived in New Town until at least the 1940s. Gilbert worked as a plasterer and died around 1905. His son, Henry, and Henry’s son were members of the Odd Fellows. Segregation ended in Blacksburg in the late 1960s. A few years later, Prices Fork Road was extended to meet Main Street, and commercial development eventually displaced the block’s last remaining homes. Today, the St. Luke and Odd Fellows Hall and the hydrant that served the neighborhood for decades are the only original structures still standing in New Town. 

A stream creeps behind the parking garage into pipes along Prices Fork Road. There the water joins the Webb Branch of Stroubles Creek, originating at the northern edge of town, and flows into the Duck Pond.

Take action! Sweep sidewalks and driveways instead of hosing them off. Washing the pavement flushes sediment and other pollutants into the stream.
