


let mapbox = document.querySelector(".is-two-thirds");
let hideButton = document.querySelector('.hide-button')
let hideDiv = document.querySelector('.hide-div')

hideButton.addEventListener('click', hideMap);

function hideMap() {
    mapbox.classList.toggle('hide-this');
    hideDiv.classList.toggle('un-sticky')
    hideButton.classList.toggle('button-margin')
    let mm = hideButton.textContent;
    let hide = "Hide Map";
    let show = "Show Map"
   
    if(mm == hide) {
        hideButton.textContent = show
    } else if (mm == show) {
        hideButton.textContent = hide
    }
}

let marker = document.querySelector('.my-frogs');
marker.addEventListener('click', showIfHidden);

function showIfHidden(e) {
 if(!e.target.closest('.frog-map-icon')) {
     return
 } 

 if(mapbox.classList.contains('hide-this')) {
     mapbox.classList.toggle('hide-this');
     hideDiv.classList.toggle('un-sticky')

     if(hideButton.textContent == "Show Map") {
         hideButton.textContent = "Hide Map"
     }
     
 }

    

}


